# WordPress Starter Template

A detailed description on how to setup a project, deployment and a list of all included WordPress plugin can be found on [Confluence](https://storydriven.atlassian.net/wiki/spaces/SW/pages/190742529/Wordpress+Setup+-+NEW). 

## Features  

* Pre-defined development workflow using a single script:
    * `develop init` - setup environment specific docker-compose and .env files
    * `develop create` - setup a wordpress project with bedrock and sage
    * `develop install` - install composer and yarn dependencies
    * `develop backup [up|down|create|load] [path-to-file]` - load or create wordpress backup, send or retrieve backups from Digital Ocean Space
    * `develop [yarn|wp|composer|s3cmd]` - pass thru to the docker container

* Docker container with all necessary tools
    * based on the the offical [php 7.3](https://hub.docker.com/_/php) Dockerfile using php-fpm and nginx
    * [Composer](http://getcomposer.org)
    * [WP-CLI](https://wp-cli.org/)
    * [Yarn 1.22.4](https://classic.yarnpkg.com/en/) and [Node 10.x](https://github.com/nodesource/distributions/blob/master/README.md) 
    * [Browsersync](https://browsersync.io/) running on Port 3000

* [Wordpress 5.3.2](https://wordpress.org/support/wordpress-version/version-5-3-2/) setup using [Bedrock 1.13.1](https://github.com/roots/bedrock) and [Sage 9.0.9](https://github.com/roots/sage)

* [Bootstrap 4.3.1](https://getbootstrap.com/docs/4.3/getting-started/introduction/) and [jQuery 3.3.1](https://blog.jquery.com/2018/01/20/jquery-3-3-1-fixed-dependencies-in-release-tag/)

* [s3tools](https://s3tools.org/) to sync with [Digital Ocean Spaces](https://www.digitalocean.com/products/spaces/) via command line

* [Bitbucke pipeline](https://bitbucket.org/product/de/features/pipelines) integration and auto deployment using bitbucket repository variables

* Curated colletion of wordpress plugins inlcuding the [classic editor](https://wordpress.org/plugins/classic-editor/) 

## Requirements  

* Docker -- [Install](https://www.docker.com/community-edition#/download)  
* Docker Compose -- [Install](https://docs.docker.com/compose/install/)

