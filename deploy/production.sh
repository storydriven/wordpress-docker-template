#!/bin/bash

cd $PROJECT_FOLDER

git fetch --all
git reset --hard origin/production

docker pull $DOCKER_SERVER/$DOCKER_IMAGE_NAME:production
docker stop $DOCKER_CONTAINER_NAME
docker rm $DOCKER_CONTAINER_NAME
docker-compose up --no-recreate -d
docker cp ./envs/production/.env $DOCKER_CONTAINER_NAME:/var/www/html/