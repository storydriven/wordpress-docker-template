#!/usr/bin/env bash

source "${SCRIPT_PATH}/config"

YEL='\033[1;33m'
NC='\033[0m'
printf "${YEL}Warning:${NC} Are you sue you want to reset the directory?\n"
read -p "[y/N] " -n 1 -r

if [[ ! $REPLY =~ ^[Yy]$ ]]; then
   printf "\n"
   [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 
else

    $COMPOSE kill
    $COMPOSE down
    docker system prune

    rm -rf $SCRIPT_PATH/src
    rm -rf $SCRIPT_PATH/envs/*/
    rm -f docker-compose.yml
    rm -f config
fi