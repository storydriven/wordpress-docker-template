#!/usr/bin/env bash

source "${SCRIPT_PATH}/config"

# define local path for backups
DATE="$(date +'%Y-%m-%d')"
TIME="$(date +'%H%M%S')"
BACKUP_PATH="src/web/app/backups/${DATE}"
ARCHIVE_NAME="${TIME}-${PROJECT_NAME}-${ENVIRONMENT}"

# path to WordPress installations
UPLOADS_FOLDER="src/web/app/uploads"

# S3 bucket
BUCKET="s3://storydriven-wp"
S3DIR="${BUCKET}/${PROJECT_NAME}"

# make sure the backup folder exists
mkdir -p ${BACKUP_PATH}

if [ "$2" == "create" ]; then
    printf "Creating backups for ${PROJECT_NAME}\n\n"

    cd "${SCRIPT_PATH}/${UPLOADS_FOLDER}"
    tar -czf "${SCRIPT_PATH}/${BACKUP_PATH}/uploads.tar.gz" \
        -C "${SCRIPT_PATH}/${UPLOADS_FOLDER}" .

    cd ${SCRIPT_PATH}
    #back up the WordPress database
    $COMPOSE run --rm $TTY \
        -w /var/www/html/ \
        ${DOCKER_CONTAINER} \
        wp db export "database.sql" \
        --single-transaction --quick --lock-tables=false --skip-themes --skip-plugins --allow-root

    cat src/database.sql | gzip > "${BACKUP_PATH}/database.sql.gz"
    rm src/database.sql

    cd ${SCRIPT_PATH}/${BACKUP_PATH}
    tar -czf "${ARCHIVE_NAME}.tar.gz" \
        uploads.tar.gz database.sql.gz 

    rm -f uploads.tar.gz database.sql.gz

elif [ "$2" == "up" ]; then
    printf "Uploading backups for ${PROJECT_NAME} to ${S3DIR}\n\n"

    $COMPOSE run --rm $TTY \
        -w "/var/www/html/web/app/backups/" \
        ${DOCKER_CONTAINER} \
        /bin/bash -c "s3cmd -q -c /var/www/html/.s3cfg sync /var/www/html/web/app/backups ${S3DIR}/" \
        2>/dev/null

elif [ "$2" == "down" ]; then
    printf "Downloading backups for ${PROJECT_NAME} from ${S3DIR}\n\n"

    $COMPOSE run --rm $TTY \
        -w "/var/www/html/web/app/backups/" \
        ${DOCKER_CONTAINER} \
        /bin/bash -c "s3cmd -q -c /var/www/html/.s3cfg sync ${S3DIR}/backups /var/www/html/web/app/" \

elif [ "$2" == "load" ]; then
    if [[ ! -f "$3" ]]; then
        RED='\033[0;31m'
        NC='\033[0m'
        printf "${RED}Error:${NC} Cannot find backup $3\n"
        exit
    fi

    mkdir -p /tmp/${PROJECT_NAME}/backup
    tar -xzf $3 -C /tmp/${PROJECT_NAME}/backup
    cd /tmp/${PROJECT_NAME}/backup

    if [[ -f "uploads.tar.gz" ]]; then
        printf "Loading media files\n"
        tar -xzf uploads.tar.gz -C "${SCRIPT_PATH}/${UPLOADS_FOLDER}"
        chmod -R 777 ${SCRIPT_PATH}/${UPLOADS_FOLDER}
    fi

    if [[ -f "database.sql.gz" ]]; then
        printf "Loading media files\n\n"

        gzip -d < database.sql.gz > ${SCRIPT_PATH}/src/database.sql

        cd $SCRIPT_PATH
        $COMPOSE run --rm $TTY \
            -w /var/www/html/ \
            ${DOCKER_CONTAINER} \
            wp db import database.sql --allow-root \
            2>/dev/null
    fi

    rm -rf /tmp/${PROJECT_NAME}/backup
fi
# delete local backups
# rm -rf ${BACKUP_PATH}/*