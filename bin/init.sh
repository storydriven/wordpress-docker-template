#!/usr/bin/env bash

source "${SCRIPT_PATH}/config"

# ask before we override existing env folder
if [[ -d "${SCRIPT_PATH}/envs/${ENVIRONMENT}" ]]; then
   YEL='\033[1;33m'
   NC='\033[0m'
   printf "${YEL}Warning:${NC} Are you sue you want to override your .env and docker-compose files?\n"
   read -p "[y/N] " -n 1 -r

   if [[ ! $REPLY =~ ^[Yy]$ ]]; then
      printf "\n"
      [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 
   fi
fi

if [ ! -d "${SCRIPT_PATH}/envs/${ENVIRONMENT}/" ]; then
    mkdir -p "${SCRIPT_PATH}/envs/${ENVIRONMENT}"
fi

cp "${SCRIPT_PATH}/envs/template.db.env" \
   "${SCRIPT_PATH}/envs/${ENVIRONMENT}/db.env"

cp "${SCRIPT_PATH}/envs/template.env" \
   "${SCRIPT_PATH}/envs/${ENVIRONMENT}/.env"

cp "${SCRIPT_PATH}/docker/docker-compose.template.yml" \
   "${SCRIPT_PATH}/docker/docker-compose.${ENVIRONMENT}.yml"

cp "${SCRIPT_PATH}/envs/s3cfg.template" \
   "${SCRIPT_PATH}/envs/${ENVIRONMENT}/.s3cfg"

sed '/^[[:blank:]]*#/d;s/#.*//' ./config | while read line ;
do
    name=${line%%=*}
    value=${line##*=}

    # auto generate passwords and salts when empty
    if [[ $name == *"_PASSWORD" && ${#value} -lt 1 ]]; then
    value=$(openssl rand -hex 16)
    
    elif [[ $name == *"_SALT" && ${#value} -lt 1 ]]; then
    value=$(openssl rand -base64 64 | tr -d '\n')

    elif [[ $name == *"_KEY" && ${#value} -lt 1 ]]; then
    value=$(openssl rand -base64 62 | tr -d '\n') 
    fi
    
    # escape special chars
    value=$(sed -e 's/[&\\/]/\\&/g; s/$/\\/' -e '$s/\\$//' <<< "$value")

    sed -i '' "s/\[${name}\]/${value}/g" "${SCRIPT_PATH}/docker/docker-compose.${ENVIRONMENT}.yml"
    sed -i '' "s/\[${name}\]/${value}/g" "${SCRIPT_PATH}/envs/${ENVIRONMENT}/.env"       
    sed -i '' "s/\[${name}\]/${value}/g" "${SCRIPT_PATH}/envs/${ENVIRONMENT}/db.env" 
    sed -i '' "s/\[${name}\]/${value}/g" "${SCRIPT_PATH}/envs/${ENVIRONMENT}/.s3cfg" 

done
