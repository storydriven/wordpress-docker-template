#!/usr/bin/env bash

source "${SCRIPT_PATH}/config"

if [ -d "${SCRIPT_PATH}/src/web/app/themes/${PROJECT_NAME}" ]; then
    RED='\033[0;31m'
    NC='\033[0m'
    printf "${RED}Error:${NC} Theme '${PROJECT_NAME}' already exists!\n" 
    exit
fi

if [ ! -d "${SCRIPT_PATH}/src" ]; then
    mkdir -p "${SCRIPT_PATH}/src"
fi

$COMPOSE build

$COMPOSE run --rm $TTY \
    -w /var/www/html/ \
    ${DOCKER_CONTAINER} \
    composer -n create-project roots/bedrock . 1.13.1 --prefer-dist

$COMPOSE run --rm $TTY \
    -w /var/www/html/web/app/themes/ \
    ${DOCKER_CONTAINER} \
    composer -n create-project roots/sage ${PROJECT_NAME} 9.0.9 --prefer-dist

$COMPOSE run --rm $TTY \
    -w /var/www/html/web/app/themes/${PROJECT_NAME} \
    ${DOCKER_CONTAINER} \
    yarn update && yarn install

$COMPOSE up -d

$COMPOSE run --rm $TTY \
    -w /var/www/html/web/wp \
    ${DOCKER_CONTAINER} \
    wp core install --allow-root \
    --url="${DOMAIN}:${PORT}" \
    --title=${PROJECT_NAME} \
    --admin_user=admin \
    --admin_email=admin@local.host \
    --admin_password=admin

$COMPOSE run --rm $TTY \
    -w /var/www/html/web/wp \
    ${DOCKER_CONTAINER} \
    wp theme activate ${PROJECT_NAME}/resources --allow-root

sed -i '' \
    -e "s,http://example.test,${DOMAIN},g" \
    -e "s,/app/themes/sage,/app/themes/${PROJECT_NAME},g" \
    "${SCRIPT_PATH}/src/web/app/themes/${PROJECT_NAME}/resources/assets/config.json"

$COMPOSE restart