#!/usr/bin/env bash

source "${SCRIPT_PATH}/config"

if [ ! -d "${SCRIPT_PATH}/src/web/app/themes/${PROJECT_NAME}/" ]; then
    RED='\033[0;31m'
    NC='\033[0m'
    printf "${RED}Error:${NC} Theme '${PROJECT_NAME}' doesn't exists!\n" 
    exit
fi

$COMPOSE run --rm $TTY \
-w /var/www/html/ \
${DOCKER_CONTAINER} \
composer update && composer install

$COMPOSE run --rm $TTY \
-w /var/www/html/web/app/themes/${PROJECT_NAME} \
${DOCKER_CONTAINER} \
composer update && composer install 

$COMPOSE run --rm $TTY \
-w /var/www/html/web/app/themes/${PROJECT_NAME} \
${DOCKER_CONTAINER} \
yarn update && yarn install 

sed -i '' \
    -e "s,http://example.test,${DOMAIN},g" \
    -e "s,/app/themes/sage,/app/themes/${PROJECT_NAME},g" \
    "${SCRIPT_PATH}/src/web/app/themes/${PROJECT_NAME}/resources/assets/config.json"
