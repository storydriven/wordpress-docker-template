#!/usr/bin/env bash

# PROJECT_NAME=$1 (build-args)
# ENVIRONMENT=$2 (build-args)

# Copy config files
cp -f /config/nginx.default /etc/nginx/sites-available/default
cp -f /config/php-fpm.conf /usr/local/etc/php-fpm.conf

if [ "$2" == "development" ]; then
    cp -f /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini
    cp -f /config/php.uploads.ini /usr/local/etc/php/conf.d/uploads.ini
else
    cp -f /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
fi

cp -f /config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Set access rights for upload folders
mkdir -p /var/www/html/web/app/plugins/all-in-one-wp-migration/storage
chown $(whoami):www-data -R /var/www/html/web/app/plugins/all-in-one-wp-migration/storage && chmod -R 777 /var/www/html/web/app/plugins/all-in-one-wp-migration/storage
mkdir -p /var/www/html/web/app/uploads
chown $(whoami):www-data -R /var/www/html/web/app/uploads && chmod -R 777 /var/www/html/web/app/uploads

# run nginx and php-fpm
/usr/bin/supervisord

cd /var/www/html/
composer update && composer install

cd /var/www/html/web/app/themes/$1
composer update && composer install
yarn update && yarn install

if [ "$2" == "development" ]; then
    yarn run start
else
    yarn run build:production
fi